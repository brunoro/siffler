(ns siffler.test.score
  (:use siffler.score)
  (:use clojure.test))

(deftest test-note?
  ; default form
  (is true  (note? [1    :A2   1  ]))  ; simplest as possible
  (is true  (note? [0.5  :B3   0.5]))  ; duration and start can be floats
  (is true  (note? [0.5  :C#3  0.5]))  ; the pitch can be sharb

  ; wrong types and orders
  (is false (note? [0.5  "al"  0.5]))  ; no strings allowed
  (is false (note? [:B3  0.4   0.5]))  ; testing order #1
  (is false (note? [0.5  0.5   :G4]))  ; testing order #2
  (is false (note? [0.5  0.5   0.5]))  ; there should be a pich

  ; there should be three elements on the vector
  (is false (note? []))                ; not zero
  (is false (note? [0.5]))             ; not one
  (is false (note? [0.5 0.5]))         ; not two
  (is false (note? [0.5 0.5 0.5 0.5])) ; not four

  ; it should be a vector!
  (is false (note? nil))              ; not nil
  (is false (note? :start))           ; not a keyword
  (is false (note? '(0.5  0.5 :G4)))  ; not a list
  (is false (note? #{0.5 :G4 0.7}))   ; not a set
  (is false (note? { :start      0.5  ; not a map
                     :duration   0.5
                     :pitch      :G4 })))  
