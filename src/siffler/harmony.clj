(ns siffler.harmony
  (:use [overtone.live]
        [overtone.inst.sampled-piano]))

(defn as-root-chord
  "transforms a chord in relative intervals from the lowest note"
  [c]
  (set (map #(- % (reduce min c)) c)))

(defn chord-is
  "checks if x is chord-type chord" 
  [chord-type x]
  (= x (resolve-chord chord-type)))

(defn remove7
  "removes the 7th from a chord, assuming it is the highest note"
  [c]
  (disj c (reduce max c)))

(defn chord-from-scale
  "return the basic chord for the provided degree on the scale"
  [scale degree]
  (let [dscale (concat scale (map #(+ 12 %) (drop 1 scale)))
        interv (map #(+ % degree) (range 0 6 2))]
    (map #(nth dscale %) interv)))

(defn progression-from-scale
  "return a list of chords for the provided degrees on the scale"
  [scale degrees]
  (map #(chord-from-scale scale %) degrees))

(defn major?
  "tells if a chord is major"
  [c]
  (contains? c 4))

(defn scale-from-chord-naive
  "returns a major scale if the chord has a major 3rd
   or a minor if the chord has a minor 3rd"
  [c]
  (let [root (reduce min c)]  
    (if (major? c) ; if it isn't minor it is major :P
      (scale root :minor)
      (scale root :major))))

(defn scale-from-chord-penta
  "returns a major scale if the chord has a major 3rd
   or a minor if the chord has a minor 3rd"
  [c]
  (let [root (reduce min c)]  
    (if (major? c) ; if it isn't minor it is major :P
      (scale root :minor-pentatonic)
      (scale root :major-pentatonic))))

(defn dominant 
  "gets the dominant from the provided chord's scale" 
  [c]
  (let [scale (scale-from-chord-naive c)]
    (chord-from-scale scale 5)))

(defn seventh
  "adds a seventh to the chord according to its scale"
  [c]
  (let [scale (scale-from-chord-naive c)]
    (conj c (nth scale 6))))

(def steedman-grammar
  [{:from [:x]
    :to [:x :x]
    :if []}
   {:from [:x :x]
    :to   ['(dominant :x) :x]
    :if   []}])
  ; {:from [:x7]
  ;  :to ['(remove7 :x7) :x7]
  ;  :if ['(chord-is :major7 :x7)]}])

(defn eval-with-vars
  "eval the expression replacing the keywords with the values
   on the variables map"
  [expression variables]
  (if (list? expression)
    (eval (map (fn [s] 
           (if (contains? variables s) 
            (variables s)
            s )) expression))
    (variables expression)))

(defn transpose-chord
  "transpose a chord by n steps"
  [c n]
  (set (map #(+ n %) c)))

(defn replace-chords
  "replace chords using a production, checking the condition first"
  [targets prod]
  (let [root (reduce min (flatten targets))
        vars (zipmap (prod :from)
                     (map as-root-chord targets))]
    (if (every? true? (map #(eval-with-vars % vars) (prod :if)))
        (map #(transpose-chord % root)
          (map #(eval-with-vars % vars) (prod :to)))
      (map set targets))))

(defn expand-progression
  "expand a progression according to a grammar"
  [prog grammar]
  (let [prod (rand-nth grammar)
        pnum (count (prod :from))
        head (take pnum prog)
        tail (drop pnum prog)]
  (if (empty? tail)
    (replace-chords head prod)
    (concat (replace-chords head prod) 
            (expand-progression tail grammar)))))

;3a. w x7    -> Dx7 x7
;    w x7    -> Dxm7 x7
;
;3b. w xm7   -> Dx7 xm7

;4.  Dx7 x7  -> Stbx7 x7
;    Dx7 x   -> Stbx x
;    Dx7 xm7 -> Stbxm7 xm7
