(ns siffler.player
  (:use [siffler.harmony]
        [overtone.live]
        [overtone.inst.sampled-piano]))

(defn play-chord
  "plays a chord with a given instrument"
  [c inst]
  (doseq [n c] (inst n))) 

(defn bass-note
  "the base note of a chord on a lower octave"
  [c]
  (- (reduce min c) 24))

(defn bass-fifth
  "the base note of a chord on a lower octave"
  [c]
  (let [fc (second (sort (seq c)))]
    (- fc 12)))

(defn play-progression
  [beat metro prog rhythm improviser]
  (let [next-beat (+ beat 4)
        ch        (first prog)
        scale     (scale-from-chord-naive ch)]
    (rhythm ch beat metro sampled-piano)
    (improviser ch beat metro sampled-piano)
    (apply-at (metro next-beat) #'play-progression next-beat 
                                                   metro 
                                                   (rest prog)
                                                   rhythm
                                                   improviser
                                                   [])))
(defn improviser1
  [c beat metro inst]
  (let [scale (scale-from-chord-penta c)]
    (doseq [i (range 4)] ; measure
      (let [qnt (rand-int 2)] ; tempo
        (doseq [t (range qnt)]
          (if (> 0.2 (rand))
            (at (metro (+ i (/ t qnt) beat))
              (inst (rand-nth scale)))))))))

(defn improviser1a
  [c beat metro inst]
  (let [scale (scale :C4 :major)];(scale-from-chord-naive c)]
    (doseq [i (range 4)] ; measure
      (let [qnt 4] ; tempo
        (doseq [t (range qnt)]
          (if (> 0.2 (rand))
            (at (metro (+ i (/ t qnt) beat))
              (let [n (rand (count scale))]
                   (inst (nth scale n))
                   (inst (nth scale (mod (+ 4 n) (count scale))))))))))))

(defn improviser2
  [c beat metro inst]
  (let [scale (scale-from-chord-penta c)]
    (doseq [i (range 4)] ; measure
      (let [qnt 8] ; tempo
        (doseq [t (range qnt)]
          (if (> 0.1 (rand))
            (at (metro (+ i (/ t qnt) beat))
              (inst (rand-nth scale)))))))))

(defn rhythm1
  [ch beat metro inst]
  (at (metro (+ 0 beat)) (inst (bass-note ch)))
  (at (metro (+ 2 beat)) (play-chord ch inst))
  (at (metro (+ 3.5 beat)) (inst (bass-fifth ch))))

(defn rhythm2
  [ch beat metro inst]
  (at (metro (+ 0 beat))    (inst (rand-nth (seq ch))))
  (at (metro (+ 1 beat))    (inst (rand-nth (seq ch))))
  (at (metro (+ 1.5 beat))  (inst (bass-note ch)))
  (at (metro (+ 2 beat))    (inst (rand-nth (seq ch))))
  (at (metro (+ 2.5 beat))  (inst (bass-note ch)))
  (at (metro (+ 2.75 beat)) (inst (bass-fifth ch)))
  (at (metro (+ 3 beat))    (inst (rand-nth (seq ch))))
  (at (metro (+ 3.5 beat))  (inst (rand-nth (seq ch)))))

(stop)

(def simple-progression1
  (progression-from-scale 
    (scale :B4 :major) 
    (map degree->int (flatten
                       (repeat 10 [:ii  :iii :iv :v])))))

(def expanded-simple1
  (expand-progression simple-progression1 steedman-grammar)) 

(println (map (fn [a] (map find-note-name (sort a))) expanded-simple1))

(def simple-progression2
  (progression-from-scale 
    (scale :C4 :major) 
    (map degree->int (flatten
                       (repeat 5 [:i   :ii  :iv  :v 
                                  :iii :v   :ii  :v])))))

(def expanded-simple2
  (expand-progression simple-progression1 steedman-grammar)) 

(println (map (fn [a] (map find-note-name (sort a))) expanded-simple2))

(stop)
(def metro (metronome 120))
(play-progression (metro) metro expanded-simple1 improviser1 rhythm1)
;(play-progression (metro) metro expanded-simple1 improviser1a rhythm1)
;(play-progression (metro) metro expanded-simple2 improviser2 rhythm2)
